import "./App.css";
import logo from "./component/statics/imagegs/svg/logo-bitbarg.svg";

function App() {
  return (
    <div className="container">
      <header>
        <div className="main-navbar">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            class="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"
            />
          </svg>
          <a className="btn-log-in" href="">
            ثبت نام
          </a>
        </div>
      </header>
      <div className="container-main-mid">
        <div className="logo">
          <img style={{ width: "272px" }} src={logo} />
        </div>
        <div className="container-row-inputs">
          <div class="cont-select-currency">
            <input className="select-currency" type="text" />
            <div className="absolute-select-currency">انتخاب ارز</div>
          </div>
          <input className="number-currency" type="number" />
          <div className="absolute-number-currency">واحد</div>
          <input className="price-currency" type="text" />
          <div className="absolute-price-currency">تومان</div>
        </div>
      </div>
    </div>
  );
}

export default App;
